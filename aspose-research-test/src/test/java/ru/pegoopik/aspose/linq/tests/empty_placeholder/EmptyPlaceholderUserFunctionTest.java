package ru.pegoopik.aspose.linq.tests.empty_placeholder;

import ru.pegoopik.aspose.linq.base.BaseTestCase;

import java.util.Map;

public class EmptyPlaceholderUserFunctionTest extends BaseTestCase {
    protected Map<String, Object> getAdditionalPlaceholders() {
        return null;
    }

    protected String getTemplateResourcePath() {
        return "linq/tests/empty_placeholder/empty_placeholder_user_function.html";
    }
}
