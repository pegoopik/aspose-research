package ru.pegoopik.aspose.linq.tests.empty_placeholder;

import com.aspose.words.Document;
import com.aspose.words.ReportBuildOptions;
import com.aspose.words.ReportingEngine;
import lombok.SneakyThrows;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class EmptyVarTest {

    private static final String RESOURCE_PATH = "linq/tests/empty_placeholder/empty_var.html";
    private Document doc;
    private ReportingEngine engine;

    @SneakyThrows
    @Before
    public void before() {
        String inDocPath = Thread.currentThread().getContextClassLoader().getResource(
                RESOURCE_PATH).getPath();
        doc = new Document(inDocPath);
        engine = new ReportingEngine();
        engine.setOptions(ReportBuildOptions.ALLOW_MISSING_MEMBERS);
        engine.getKnownTypes().add(Iterable.class);
    }

    @SneakyThrows
    @Test
    public void testNotEmptyCollection() {
        Map<String, Object> placeholders = new HashMap<String, Object>();
        placeholders.put("BCS", "Broker Credit Service");
        placeholders.put("collection", new ArrayList()); //fill placeholder 'collection'
        Object[] values = new Object[placeholders.size()];
        String[] names = new String[placeholders.size()];
        int i = 0; for(Map.Entry<String, Object> placeholder : placeholders.entrySet()) {
            names[i] = placeholder.getKey();
            values[i] = placeholder.getValue();
            i++;
        }
        engine.buildReport(doc, values, names); //SUCCESS
    }

    @SneakyThrows
    @Test
    public void testEmptyCollection() {
        Map<String, Object> placeholders = new HashMap<String, Object>();
        placeholders.put("BCS", "Broker Credit Service");
        Object[] values = new Object[placeholders.size()];
        String[] names = new String[placeholders.size()];
        int i = 0; for(Map.Entry<String, Object> placeholder : placeholders.entrySet()) {
            names[i] = placeholder.getKey();
            values[i] = placeholder.getValue();
            i++;
        }
        engine.buildReport(doc, values, names); //java.lang.IllegalStateException
    }

}
