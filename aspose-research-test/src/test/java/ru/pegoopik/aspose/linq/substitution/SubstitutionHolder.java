package ru.pegoopik.aspose.linq.substitution;

public class SubstitutionHolder {

    public String simpleTestFunction(Object... args) {
        StringBuilder sb = new StringBuilder();
        for (Object o : args) {
            if (o != null) {
                sb.append(o).append("; ");
            }
        }
        return new String(sb);
    }

}
