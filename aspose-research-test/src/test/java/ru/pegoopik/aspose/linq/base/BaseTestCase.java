package ru.pegoopik.aspose.linq.base;

import com.aspose.words.Document;
import com.aspose.words.ReportingEngine;
import lombok.SneakyThrows;
import org.junit.Test;
import ru.pegoopik.aspose.linq.substitution.SubstitutionHolder;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

public abstract class BaseTestCase {

    @SneakyThrows
    @Test
    public void testBody() {
        String inDocPath = Thread.currentThread().getContextClassLoader().getResource(getTemplateResourcePath()).getPath();
        Document doc = new Document(inDocPath);
        ReportingEngine engine = new ReportingEngine();
        engine.getKnownTypes().add(Date.class);
        engine.getKnownTypes().add(SubstitutionHolder.class);
        Map<String, Object> placeholders = getAdditionalPlaceholders();
        if (placeholders == null) {
            placeholders = new HashMap<String, Object>();
        }
        placeholders.put("SH", new SubstitutionHolder());//User Functions
        Object[] values = new Object[placeholders.size()];
        String[] names = new String[placeholders.size()];
        int i = 0; for(Map.Entry<String, Object> placeholder : placeholders.entrySet()) {
            names[i] = placeholder.getKey();
            values[i] = placeholder.getValue();
        }
        engine.buildReport(doc, values, names);//IllegalArgumentException
    }

    protected abstract Map<String, Object> getAdditionalPlaceholders();

    protected abstract String getTemplateResourcePath();
}
