package ru.pegoopik.aspose.linq.tests;

import com.aspose.words.Document;
import com.aspose.words.ReportingEngine;
import org.junit.Test;

import java.util.Date;

public class DateFormatTest {

    @Test
    public void test() throws Exception {
        String inDocPath = Thread.currentThread().getContextClassLoader().getResource("linq/tests/date_format.docx").getPath();
        Document doc = new Document(inDocPath);
        ReportingEngine engine = new ReportingEngine();
        engine.getKnownTypes().add(Date.class);
        engine.buildReport(doc, new Date(), "dateTime");//IllegalArgumentException
    }
}
