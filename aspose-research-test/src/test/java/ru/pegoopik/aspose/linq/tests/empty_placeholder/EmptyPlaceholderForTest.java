package ru.pegoopik.aspose.linq.tests.empty_placeholder;

import ru.pegoopik.aspose.linq.base.BaseTestCase;

import java.util.Map;

public class EmptyPlaceholderForTest extends BaseTestCase {
    protected Map<String, Object> getAdditionalPlaceholders() {
        return null;
    }

    protected String getTemplateResourcePath() {
        return "linq/tests/empty_placeholder/empty_placeholder_for.html";
    }
}
