package ru.pegoopik.aspose;

import org.junit.Test;

import java.util.Map;
import java.util.TreeMap;

public class ShowRemoteSystemPropsTest {

    @Test
    public void test() {
        Map<String, Object> props = new TreeMap();
        for (Map.Entry entry : System.getProperties().entrySet()) {
            props.put(entry.getKey().toString(), entry.getValue());
        }
        for (Map.Entry<String, Object> entry : props.entrySet()) {
            System.out.print(entry.getKey());
            System.out.print(" : ");
            System.out.println(entry.getValue());
        }
    }

}
